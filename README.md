# A collection of Nagios Scripts

## check_fritz 
An AVM Fritz!Box monitoring script for Nagios & LibreNMS

Usage:
```
usage: check_fritz -d -H hostname -f <function> [-w <warn>] [-c <crit>]
    -d:   enable debug output.
    -H:   hostname to check (if -H is missing it usses 'fritz.box').
    -w:   warn limit, depends on function.
    -c:   critical limit, depends on function.
    -f:   to call (if -f is missing it usses 'linkuptime').
functions:
    connection = connection status (full connection, inc, PPPoE).
    linkstatus = Link status (Physical link status to DSLAM/fiberswitch).
    linktype   = Link type (DSL, Ehernet, Fiber).
    linkuptime = Link uptime in seconds & days-hours,minutes-seconds.
    ipaddress  = external IPv4 address.
    upload     = Current used upload bandwidth.
    download   = Current used download bandwidth.
    uplink     = Current Trained uplink bandwidth.
    downlink   = Current Trained downlink bandwidth.
```

Others will come... if i need them... and have the time...

** By Spookje - [https://gitlab.com/Spookje/](https://gitlab.com/Spookje/) - [https://spookje.org/](https://spookje.org/) **